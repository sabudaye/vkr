defmodule IpLab.InventoryTest do
  use IpLab.DataCase

  alias IpLab.Inventory

  describe "category" do
    alias IpLab.Inventory.Category

    @valid_attrs %{
      name: "some name",
      properties: "some properties"
    }
    @update_attrs %{
      name: "some updated name",
      properties: "some updated properties"
    }
    @invalid_attrs %{name: nil, properties: nil}

    def category_fixture(attrs \\ %{}) do
      {:ok, category} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Inventory.create_category()

      category
    end

    test "list_category/0 returns all category" do
      category = category_fixture()
      assert Inventory.list_category() == [category]
    end

    test "get_category!/1 returns the category with given id" do
      category = category_fixture()
      assert Inventory.get_category!(category.id) == category
    end

    test "create_category/1 with valid data creates a category" do
      assert {:ok, %Category{} = category} = Inventory.create_category(@valid_attrs)
      assert category.name == "some name"
      assert category.properties == "some properties"
    end

    test "create_category/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Inventory.create_category(@invalid_attrs)
    end

    test "update_category/2 with valid data updates the category" do
      category = category_fixture()
      assert {:ok, %Category{} = category} = Inventory.update_category(category, @update_attrs)
      assert category.name == "some updated name"
      assert category.properties == "some updated properties"
    end

    test "update_category/2 with invalid data returns error changeset" do
      category = category_fixture()
      assert {:error, %Ecto.Changeset{}} = Inventory.update_category(category, @invalid_attrs)
      assert category == Inventory.get_category!(category.id)
    end

    test "delete_category/1 deletes the category" do
      category = category_fixture()
      assert {:ok, %Category{}} = Inventory.delete_category(category)
      assert_raise Ecto.NoResultsError, fn -> Inventory.get_category!(category.id) end
    end

    test "change_category/1 returns a category changeset" do
      category = category_fixture()
      assert %Ecto.Changeset{} = Inventory.change_category(category)
    end
  end

  describe "equipment" do
    alias IpLab.Inventory.Equipment

    @valid_category_attrs %{
      name: "some name",
      properties: "some properties"
    }

    @valid_attrs %{
      factory: "some factory",
      inventory_number: "some inventory_number",
      location: "some location",
      model_name: "some model_name",
      nomenclature_number: "some nomenclature_number",
      properties: "some properties",
      serial_number: "some serial_number",
      user: "some user"
    }
    @update_attrs %{
      factory: "some updated factory",
      inventory_number: "some updated inventory_number",
      location: "some updated location",
      model_name: "some updated model_name",
      nomenclature_number: "some updated nomenclature_number",
      properties: "some updated properties",
      serial_number: "some updated serial_number",
      user: "some updated user"
    }
    @invalid_attrs %{
      factory: nil,
      inventory_number: nil,
      location: nil,
      model_name: nil,
      nomenclature_number: nil,
      properties: nil,
      serial_number: nil,
      user: nil
    }

    def equipment_fixture(attrs \\ %{}) do
      {:ok, equipment} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Inventory.create_equipment()

      equipment
    end

    setup do
      {:ok, category} = Inventory.create_category(@valid_category_attrs)
      {:ok, valid_attrs: Map.merge(@valid_attrs, %{category_id: category.id})}
    end

    test "list_equipment/0 returns all equipment", %{valid_attrs: va} do
      equipment_fixture(va)
      assert [%Equipment{}] = Inventory.list_equipment()
    end

    test "get_equipment!/1 returns the equipment with given id", %{valid_attrs: va} do
      equipment = equipment_fixture(va)
      assert Inventory.get_equipment!(equipment.id) == equipment
    end

    test "create_equipment/1 with valid data creates a equipment", %{valid_attrs: va} do
      assert {:ok, %Equipment{} = equipment} = Inventory.create_equipment(va)
      assert equipment.factory == "some factory"
      assert equipment.inventory_number == "some inventory_number"
      assert equipment.location == "some location"
      assert equipment.model_name == "some model_name"
      assert equipment.nomenclature_number == "some nomenclature_number"
      assert equipment.properties == "some properties"
      assert equipment.serial_number == "some serial_number"
      assert equipment.user == "some user"
    end

    test "create_equipment/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Inventory.create_equipment(@invalid_attrs)
    end

    test "update_equipment/2 with valid data updates the equipment", %{valid_attrs: va} do
      equipment = equipment_fixture(va)

      assert {:ok, %Equipment{} = equipment} =
               Inventory.update_equipment(equipment, @update_attrs)

      assert equipment.factory == "some updated factory"
      assert equipment.inventory_number == "some updated inventory_number"
      assert equipment.location == "some updated location"
      assert equipment.model_name == "some updated model_name"
      assert equipment.nomenclature_number == "some updated nomenclature_number"
      assert equipment.properties == "some updated properties"
      assert equipment.serial_number == "some updated serial_number"
      assert equipment.user == "some updated user"
    end

    test "update_equipment/2 with invalid data returns error changeset", %{valid_attrs: va} do
      equipment = equipment_fixture(va)
      assert {:error, %Ecto.Changeset{}} = Inventory.update_equipment(equipment, @invalid_attrs)
      assert equipment == Inventory.get_equipment!(equipment.id)
    end

    test "delete_equipment/1 deletes the equipment", %{valid_attrs: va} do
      equipment = equipment_fixture(va)
      assert {:ok, %Equipment{}} = Inventory.delete_equipment(equipment)
      assert_raise Ecto.NoResultsError, fn -> Inventory.get_equipment!(equipment.id) end
    end

    test "change_equipment/1 returns a equipment changeset", %{valid_attrs: va} do
      equipment = equipment_fixture(va)
      assert %Ecto.Changeset{} = Inventory.change_equipment(equipment)
    end
  end
end
