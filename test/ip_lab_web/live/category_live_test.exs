defmodule IpLabWeb.CategoryLiveTest do
  use IpLabWeb.ConnCase

  import Phoenix.LiveViewTest

  alias IpLab.Inventory

  @create_attrs %{name: "some name", properties: "some properties"}
  @update_attrs %{
    name: "some updated name",
    properties: "some updated properties"
  }
  @invalid_attrs %{name: nil, properties: nil}

  defp fixture(:category) do
    {:ok, category} = Inventory.create_category(@create_attrs)
    category
  end

  defp create_category(_) do
    category = fixture(:category)
    %{category: category}
  end

  describe "Index" do
    setup [:register_and_log_in_user, :create_category]

    test "lists all category", %{conn: conn, category: category} do
      {:ok, _index_live, html} = live(conn, Routes.category_index_path(conn, :index))

      assert html =~ "Категории"
      assert html =~ category.name
    end

    test "saves new category", %{conn: conn} do
      {:ok, index_live, _html} = live(conn, Routes.category_index_path(conn, :index))

      assert index_live |> element("a[href=\"/categories/new\"]") |> render_click() =~
               "Добавить"

      assert_patch(index_live, Routes.category_new_path(conn, :new))

      {:ok, new_live, _html} = live(conn, Routes.category_new_path(conn, :new))

      assert new_live
             |> form("#category-form", category: @invalid_attrs)
             |> render_change() =~ "can&apos;t be blank"

      {:ok, _, html} =
        new_live
        |> form("#category-form", category: @create_attrs)
        |> render_submit()
        |> follow_redirect(conn, Routes.category_index_path(conn, :index))

      assert html =~ "Категории"
      assert html =~ "some name"
    end

    test "updates category in listing", %{conn: conn, category: category} do
      {:ok, index_live, _html} = live(conn, Routes.category_index_path(conn, :index))

      assert index_live
             |> element("#category-#{category.id} a[href=\"/categories/#{category.id}/edit\"]")
             |> render_click() =~
               "Добавить"

      assert_patch(index_live, Routes.category_edit_path(conn, :edit, category))

      {:ok, edit_live, _html} = live(conn, Routes.category_edit_path(conn, :edit, category))

      assert edit_live
             |> form("#category-form", category: @invalid_attrs)
             |> render_change() =~ "can&apos;t be blank"

      {:ok, _, html} =
        edit_live
        |> form("#category-form", category: @update_attrs)
        |> render_submit()
        |> follow_redirect(conn, Routes.category_index_path(conn, :index))

      assert html =~ "Категории"
      assert html =~ "some updated name"
    end

    test "deletes category in listing", %{conn: conn, category: category} do
      {:ok, index_live, _html} = live(conn, Routes.category_index_path(conn, :index))

      assert index_live |> element("#category-#{category.id} a[href=\"#\"]") |> render_click()
      refute has_element?(index_live, "#category-#{category.id}")
    end
  end
end
