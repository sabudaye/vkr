defmodule IpLabWeb.EquipmentLiveTest do
  use IpLabWeb.ConnCase

  import Phoenix.LiveViewTest

  alias IpLab.Inventory

  @create_category_attrs %{
    name: "some name",
    properties: "some properties"
  }
  @create_attrs %{
    factory: "some factory",
    inventory_number: "some inventory_number",
    location: "some location",
    model_name: "some model_name",
    nomenclature_number: "some nomenclature_number",
    properties: "some properties",
    serial_number: "some serial_number",
    user: "some user"
  }
  @update_attrs %{
    factory: "some updated factory",
    inventory_number: "some updated inventory_number",
    location: "some updated location",
    model_name: "some updated model_name",
    nomenclature_number: "some updated nomenclature_number",
    properties: "some updated properties",
    serial_number: "some updated serial_number",
    user: "some updated user"
  }
  @invalid_attrs %{
    factory: nil,
    inventory_number: nil,
    location: nil,
    model_name: nil,
    nomenclature_number: nil,
    properties: nil,
    serial_number: nil,
    user: nil
  }

  defp fixture(:category) do
    {:ok, category} = Inventory.create_category(@create_category_attrs)
    category
  end

  defp fixture(:equipment) do
    category = fixture(:category)

    {:ok, equipment} =
      Inventory.create_equipment(Map.merge(@create_attrs, %{category_id: category.id}))

    equipment
  end

  defp create_equipment(_) do
    equipment = fixture(:equipment)
    %{equipment: equipment}
  end

  describe "Index" do
    setup [:register_and_log_in_user, :create_equipment]

    test "lists all equipment", %{conn: conn, equipment: equipment} do
      {:ok, _index_live, html} = live(conn, Routes.equipment_index_path(conn, :index))

      assert html =~ "Оборудование"
      assert html =~ equipment.factory
    end

    test "saves new equipment", %{conn: conn} do
      {:ok, index_live, _html} = live(conn, Routes.equipment_index_path(conn, :index))

      assert index_live |> element("a", "Добавить") |> render_click() =~
               "Добавить"

      assert_patch(index_live, Routes.equipment_new_path(conn, :new))

      {:ok, new_live, _html} = live(conn, Routes.equipment_new_path(conn, :new))

      assert new_live
             |> form("#equipment-form", equipment: @invalid_attrs)
             |> render_change() =~ "can&apos;t be blank"

      {:ok, _, html} =
        new_live
        |> form("#equipment-form", equipment: @create_attrs)
        |> render_submit()
        |> follow_redirect(conn, Routes.equipment_index_path(conn, :index))

      assert html =~ "Оборудование"
      assert html =~ "some factory"
    end

    test "updates equipment in listing", %{conn: conn, equipment: equipment} do
      {:ok, index_live, _html} = live(conn, Routes.equipment_index_path(conn, :index))

      assert index_live
             |> element("#equipment-#{equipment.id} a[href=\"/equipment/#{equipment.id}/edit\"]")
             |> render_click() =~
               "Добавить"

      assert_patch(index_live, Routes.equipment_edit_path(conn, :edit, equipment))

      {:ok, edit_live, _html} = live(conn, Routes.equipment_edit_path(conn, :edit, equipment))

      assert edit_live
             |> form("#equipment-form", equipment: @invalid_attrs)
             |> render_change() =~ "can&apos;t be blank"

      {:ok, _, html} =
        edit_live
        |> form("#equipment-form", equipment: @update_attrs)
        |> render_submit()
        |> follow_redirect(conn, Routes.equipment_index_path(conn, :index))

      assert html =~ "Оборудование"
      assert html =~ "some updated factory"
    end

    test "deletes equipment in listing", %{conn: conn, equipment: equipment} do
      {:ok, index_live, _html} = live(conn, Routes.equipment_index_path(conn, :index))

      assert index_live |> element("#equipment-#{equipment.id} a[href=\"#\"]") |> render_click()
      refute has_element?(index_live, "#equipment-#{equipment.id}")
    end
  end
end
