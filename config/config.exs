# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :ip_lab,
  ecto_repos: [IpLab.Repo]

# Configures the endpoint
config :ip_lab, IpLabWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "OjdSz/+b6chqM7707xKPLlnVwucARFuMavZrCVt1EpfYMI3AN1D9HWTm+R4Fb2gB",
  render_errors: [view: IpLabWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: IpLab.PubSub,
  live_view: [signing_salt: "1R8XJEzd"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
