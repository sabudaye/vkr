defmodule IpLabWeb.EquipmentLive.New do
  use IpLabWeb, :live_view

  # alias IpLab.Inventory
  alias IpLab.Inventory.Equipment

  @impl true
  def mount(_params, _session, socket) do
    {:ok, socket}
  end

  @impl true
  def handle_params(_params, _, socket) do
    {:noreply,
     socket |> assign(:page_title, "Новое оборудование") |> assign(:equipment, %Equipment{})}
  end
end
