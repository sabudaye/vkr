defmodule IpLabWeb.EquipmentLive.Index do
  use IpLabWeb, :live_view

  alias IpLab.Inventory
  alias IpLab.Inventory.Equipment

  @impl true
  def mount(_params, _session, socket) do
    {:ok, assign(socket, :equipment_collection, list_equipment())}
  end

  @impl true
  def handle_params(params, _url, socket) do
    {:noreply, apply_action(socket, socket.assigns.live_action, params)}
  end

  defp apply_action(socket, :index, _params) do
    socket
    |> assign(:page_title, "Список оборудования")
    |> assign(:equipment, nil)
  end

  @impl true
  def handle_event("delete", %{"id" => id}, socket) do
    equipment = Inventory.get_equipment!(id)
    {:ok, _} = Inventory.delete_equipment(equipment)

    {:noreply, assign(socket, :equipment_collection, list_equipment())}
  end

  @impl true
  def handle_event("search", %{"search" => search}, socket) do
    search = parse_search_params(search)

    {:noreply, assign(socket, :equipment_collection, Inventory.list_equipment_by(search))}
  end

  defp list_equipment do
    Inventory.list_equipment()
  end

  defp parse_search_params(params) do
    Enum.reject(params, fn {k, v} -> !Map.has_key?(search_keys(), k) || v == "" end)
  end

  defp search_keys() do
    keys =
      %Equipment{}
      |> Map.from_struct()
      |> Map.drop([:__meta__])
      |> Map.keys()

    str_keys = Enum.map(keys, &Atom.to_string/1)
    Map.new(Enum.zip(str_keys, keys))
  end
end
