defmodule IpLabWeb.CategoryLive.New do
  use IpLabWeb, :live_view

  # alias IpLab.Inventory
  alias IpLab.Inventory.Category

  @impl true
  def mount(_params, _session, socket) do
    {:ok, socket}
  end

  @impl true
  def handle_params(_params, _, socket) do
    {:noreply, socket |> assign(:page_title, "Новая категория") |> assign(:category, %Category{})}
  end
end
