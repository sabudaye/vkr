defmodule IpLabWeb.CategoryLive.Edit do
  use IpLabWeb, :live_view

  alias IpLab.Inventory

  @impl true
  def mount(_params, _session, socket) do
    {:ok, socket}
  end

  @impl true
  def handle_params(%{"id" => id}, _, socket) do
    {:noreply,
     socket
     |> assign(:page_title, "Редактирование категории")
     |> assign(:category, Inventory.get_category!(id))}
  end
end
