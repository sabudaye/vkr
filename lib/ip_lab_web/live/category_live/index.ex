defmodule IpLabWeb.CategoryLive.Index do
  use IpLabWeb, :live_view

  alias IpLab.Inventory

  @impl true
  def mount(_params, _session, socket) do
    {:ok, assign(socket, :category_collection, list_category())}
  end

  @impl true
  def handle_params(params, _url, socket) do
    {:noreply, apply_action(socket, socket.assigns.live_action, params)}
  end

  defp apply_action(socket, :index, _params) do
    socket
    |> assign(:page_title, "Список категорий")
    |> assign(:category, nil)
  end

  @impl true
  def handle_event("delete", %{"id" => id}, socket) do
    category = Inventory.get_category!(id)
    {:ok, _} = Inventory.delete_category(category)

    {:noreply, assign(socket, :category_collection, list_category())}
  end

  defp list_category do
    Inventory.list_category()
  end
end
