defmodule IpLab.Inventory do
  @moduledoc """
  The Inventory context.
  """

  import Ecto.Query, warn: false
  alias IpLab.Repo

  alias IpLab.Inventory.Category

  @doc """
  Returns the list of category.

  ## Examples

      iex> list_category()
      [%Category{}, ...]

  """
  def list_category do
    Repo.all(Category)
  end

  @doc """
  Gets a single category.

  Raises `Ecto.NoResultsError` if the Category does not exist.

  ## Examples

      iex> get_category!(123)
      %Category{}

      iex> get_category!(456)
      ** (Ecto.NoResultsError)

  """
  def get_category!(id), do: Repo.get!(Category, id)

  @doc """
  Creates a category.

  ## Examples

      iex> create_category(%{field: value})
      {:ok, %Category{}}

      iex> create_category(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_category(attrs \\ %{}) do
    %Category{}
    |> Category.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a category.

  ## Examples

      iex> update_category(category, %{field: new_value})
      {:ok, %Category{}}

      iex> update_category(category, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_category(%Category{} = category, attrs) do
    category
    |> Category.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a category.

  ## Examples

      iex> delete_category(category)
      {:ok, %Category{}}

      iex> delete_category(category)
      {:error, %Ecto.Changeset{}}

  """
  def delete_category(%Category{} = category) do
    Repo.delete(category)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking category changes.

  ## Examples

      iex> change_category(category)
      %Ecto.Changeset{data: %Category{}}

  """
  def change_category(%Category{} = category, attrs \\ %{}) do
    Category.changeset(category, attrs)
  end

  alias IpLab.Inventory.Equipment

  @doc """
  Returns the list of equipment.

  ## Examples

      iex> list_equipment()
      [%Equipment{}, ...]

  """
  def list_equipment do
    Equipment
    |> join(:inner, [e], c in assoc(e, :category))
    |> preload([_, c], category: c)
    |> Repo.all()
  end

  @doc """
  Gets a single equipment.

  Raises `Ecto.NoResultsError` if the Equipment does not exist.

  ## Examples

      iex> get_equipment!(123)
      %Equipment{}

      iex> get_equipment!(456)
      ** (Ecto.NoResultsError)

  """
  def get_equipment!(id), do: Repo.get!(Equipment, id)

  @doc """
  Returns the list of equipment by param.

  ## Examples

      iex> list_equipment_by(factory: "Lenovo")
      [%Equipment{}, ...]

  """
  def list_equipment_by(search_params) do
    q =
      Equipment
      |> join(:inner, [e], c in assoc(e, :category), as: :category)
      |> preload([_, c], category: c)

    q =
      Enum.reduce(search_params, q, fn
        {"id", v}, acc ->
          where(acc, id: ^cast_int(v))

        {"factory", v}, acc ->
          where(acc, [e], ilike(e.factory, ^escape(v)))

        {"inventory_number", v}, acc ->
          where(acc, [e], ilike(e.inventory_number, ^escape(v)))

        {"location", v}, acc ->
          where(acc, [e], ilike(e.location, ^escape(v)))

        {"model_name", v}, acc ->
          where(acc, [e], ilike(e.model_name, ^escape(v)))

        {"nomenclature_number", v}, acc ->
          where(acc, [e], ilike(e.nomenclature_number, ^escape(v)))

        {"properties", v}, acc ->
          where(acc, [e], ilike(e.properties, ^escape(v)))

        {"serial_number", v}, acc ->
          where(acc, [e], ilike(e.serial_number, ^escape(v)))

        {"user", v}, acc ->
          where(acc, [e], ilike(e.user, ^escape(v)))

        {"category", v}, acc ->
          where(acc, [category: c], ilike(c.name, ^escape(v)))

        {_, _}, acc ->
          acc
      end)

    Repo.all(q)
  end

  defp escape(str) do
    "%#{String.replace(str, "%", "\\%")}%"
  end

  defp cast_int(value) when is_number(value), do: value

  defp cast_int(value) do
    with {int, _binary} <- Integer.parse(value) do
      int
    else
      :error -> 0
    end
  end

  @doc """
  Creates a equipment.

  ## Examples

      iex> create_equipment(%{field: value})
      {:ok, %Equipment{}}

      iex> create_equipment(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_equipment(attrs \\ %{}) do
    %Equipment{}
    |> Equipment.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a equipment.

  ## Examples

      iex> update_equipment(equipment, %{field: new_value})
      {:ok, %Equipment{}}

      iex> update_equipment(equipment, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_equipment(%Equipment{} = equipment, attrs) do
    equipment
    |> Equipment.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a equipment.

  ## Examples

      iex> delete_equipment(equipment)
      {:ok, %Equipment{}}

      iex> delete_equipment(equipment)
      {:error, %Ecto.Changeset{}}

  """
  def delete_equipment(%Equipment{} = equipment) do
    Repo.delete(equipment)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking equipment changes.

  ## Examples

      iex> change_equipment(equipment)
      %Ecto.Changeset{data: %Equipment{}}

  """
  def change_equipment(%Equipment{} = equipment, attrs \\ %{}) do
    Equipment.changeset(equipment, attrs)
  end
end
