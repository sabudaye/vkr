defmodule IpLab.Inventory.Equipment do
  use Ecto.Schema
  import Ecto.Changeset

  schema "equipment" do
    field :factory, :string
    field :inventory_number, :string
    field :location, :string
    field :model_name, :string
    field :nomenclature_number, :string
    field :properties, :string
    field :serial_number, :string
    field :user, :string
    belongs_to :category, IpLab.Inventory.Category

    timestamps()
  end

  @doc false
  def changeset(equipment, attrs) do
    equipment
    |> cast(attrs, [
      :category_id,
      :factory,
      :model_name,
      :inventory_number,
      :nomenclature_number,
      :serial_number,
      :location,
      :user,
      :properties
    ])
    |> validate_required([
      :category_id,
      :factory,
      :model_name,
      :inventory_number,
      :nomenclature_number,
      :serial_number,
      :location,
      :user,
      :properties
    ])
  end
end
