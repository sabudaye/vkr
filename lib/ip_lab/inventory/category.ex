defmodule IpLab.Inventory.Category do
  use Ecto.Schema
  import Ecto.Changeset

  schema "categories" do
    field :name, :string
    field :properties, :string

    timestamps()
  end

  @doc false
  def changeset(category, attrs) do
    category
    |> cast(attrs, [:name, :properties])
    |> validate_required([:name])
    |> validate_length(:name, min: 2)
  end
end
