defmodule IpLab.Repo.Migrations.CreateEquipment do
  use Ecto.Migration

  def change do
    create table(:equipment) do
      add :factory, :string
      add :model_name, :string
      add :inventory_number, :string
      add :nomenclature_number, :string
      add :serial_number, :string
      add :location, :string
      add :user, :string
      add :properties, :string
      add :category_id, references(:categories, on_delete: :nothing)

      timestamps()
    end

    create index(:equipment, [:category_id])
  end
end
